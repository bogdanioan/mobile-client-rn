const baseUrl = 'http://10.144.5.115:3000/api';
export const urls = {
    login : [baseUrl,'auth','login'].join('/'),
    recipes: [baseUrl, 'recipes'].join('/')
}