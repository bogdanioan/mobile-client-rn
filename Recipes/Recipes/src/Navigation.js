import { createStackNavigator, createAppContainer } from 'react-navigation';
import { LoginScreen } from './LoginScreen'
import { ListScreen } from './ListScreen';
import { AddRecipeScreen } from './AddRecipeScreen';
let AppNavigation = createStackNavigator({
  Login: { screen: LoginScreen },
  List: ListScreen,
  Recipe: AddRecipeScreen
});

AppNavigation = createAppContainer(AppNavigation);
export default AppNavigation;