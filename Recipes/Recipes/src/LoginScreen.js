import React from 'react';
import { Text, TextInput, View, Button, AsyncStorage, AppRegistry } from 'react-native';
import { urls } from './constants';

export class LoginScreen extends React.Component {
    static navigationOptions = {
        title: 'Login'
    };
    constructor(props) {
        super(props);
        this.state = {
            credentials: {
                email: '',
                password: ''
            }
        }
    }

    async componentWillMount() {
        await AsyncStorage.getItem('user')
            .then(item => JSON.parse(item))
            .then((data) => {
                if (data) {
                    const { navigate } = this.props.navigation;
                    AsyncStorage.setItem('recipes','');
                    navigate('List', { user: data });
                }
            });

    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text>Email</Text>
                <TextInput style={{ width: 300, height: 30, borderWidth: 1, marginBottom: 20 }}
                    onChangeText={(text) => this.state.credentials.email = text}
                ></TextInput>
                <Text>Password</Text>
                <TextInput secureTextEntry={true} style={{ width: 300, height: 30, borderWidth: 1, marginBottom: 20 }}
                    onChangeText={(text) => this.state.credentials.password = text}
                ></TextInput>
                <Button
                    onPress={() => {
                        this.login();
                    }}
                    title="LOG IN"
                />
            </View>
        );
    }

    login() {
        fetch(urls.login, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(
                this.state.credentials
            ),
        }).then(response => response.json())
            .then(token => {
                AsyncStorage.setItem('user', JSON.stringify({ email: this.state.credentials.email, token: token.token }));
                const { navigate } = this.props.navigation;
                navigate('List', { user: { email: this.state.credentials.email, token: token.token } });
            })
    }

}

AppRegistry.registerComponent('Recipse', () => LoginScreen);