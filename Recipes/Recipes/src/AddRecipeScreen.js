import React from 'react';
import { Text, TextInput, View, Button, AsyncStorage, AppRegistry, NetInfo } from 'react-native';
import { urls } from './constants';
import { makeId } from './functions.js'

export class AddRecipeScreen extends React.Component {
    static navigationOptions = {
        title: 'Recipe'
    };
    constructor(props) {
        super(props);
        this.state = {
            recipe: {
                title: '',
                preparation: '',
                secretIngridient: '',
                estimatedPrice: 0
            },
            user: null
        }
    }

    async componentWillMount() {
        await AsyncStorage.getItem('user')
            .then(item => JSON.parse(item))
            .then((data) => {
                if (data) {
                    this.state.user = data;
                }
            });

    }
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text>Title</Text>
                <TextInput style={{ width: 300, height: 30, borderWidth: 1, marginBottom: 20 }}
                    onChangeText={(text) => this.state.recipe.title = text}
                ></TextInput>
                <Text>Preparation</Text>
                <TextInput style={{ width: 300, height: 30, borderWidth: 1, marginBottom: 20 }}
                    onChangeText={(text) => this.state.recipe.preparation = text}
                ></TextInput>
                <Text>Secret ingridient</Text>
                <TextInput style={{ width: 300, height: 30, borderWidth: 1, marginBottom: 20 }}
                    onChangeText={(text) => this.state.recipe.secretIngridient = text}
                ></TextInput>
                <Button
                    onPress={() => {
                        this.save();
                    }}
                    title="Save"
                />
            </View>
        );
    }

    save() {
        const user = this.state.user;
        const {navigate} = this.props.navigation;
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                fetch(urls.recipes, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${user.token}`
                    },
                    body: JSON.stringify(
                        this.state.recipe
                    ),
                }).then(response => response.json())
                    .then(recipe => {
                        this.addItemToStorage(recipe);
                        navigate('List', { user: user });
                    });
            } else {
                const recipe = this.state.recipe;
                recipe.userEmail = user.email;
                recipe._id = makeId();
                this.addItemToStorage(recipe);
                this.addItemOffline(recipe);
            }
        })

    }

    addItemOffline(recipe) {
        AsyncStorage.getItem('add-recipes')
            .then(item => JSON.parse(item))
            .then((data) => {
                if (data) {
                    data.push(recipe)
                    AsyncStorage.setItem('add-recipes', JSON.stringify(data));
                } else {
                    AsyncStorage.setItem('add-recipes', JSON.stringify([recipe]));
                }
                const { navigate } = this.props.navigation;
                const user = this.state.user;
                navigate('List', { user: user });

            });
    }
    addItemToStorage(recipe) {
        AsyncStorage.getItem('recipes')
            .then(item => JSON.parse(item))
            .then((data) => {
                if (data) {
                    data.push(recipe)
                    AsyncStorage.setItem('recipes', JSON.stringify(data));
                } else {
                    AsyncStorage.setItem('recipes', JSON.stringify([recipe]));
                }
            });
    }

}

AppRegistry.registerComponent('Recipse', () => AddRecipeScreen);