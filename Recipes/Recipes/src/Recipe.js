import React, { Component } from 'react'
import { ScrollView,
    Text,
    View,
    Button,
    AsyncStorage,
    TextInput,
    StyleSheet,
    TouchableOpacity } from 'react-native'
 
export class Recipe extends React.Component {
    recipe;
    constructor(props) {
        super(props);
        this.state = {}
        this.recipe = props.keyval;
    }

    render() {
        return (
            <View style = {styles.movie}>
                <Text style = {styles.movieText}>Title: {this.recipe.title}</Text>
                <Text style = {styles.movieText}>Preparation: {this.recipe.preparation}</Text>
                <Text style = {styles.movieText}>Secret: {this.recipe.secretIngridient}</Text>
                <Text style = {styles.movieText}>Price: {this.recipe.estimatedPrice}</Text>

                {/* <TouchableOpacity onPress = {this.props.deleteMethod}
                    style = {styles.movieDelete}>
                    <Text style = {styles.movieDeleteText}>X</Text>
                </TouchableOpacity> */}
            </View>
            )
            
    }
}


const styles = StyleSheet.create({
    movie: {
        position: 'relative',
        padding: 20,
        paddingRight: 100,
        borderBottomWidth:2,
        borderBottomColor: '#ededed'
    },
    movieText: {
        paddingLeft: 20,
        borderLeftWidth: 10,
        borderLeftColor: '#E91E63'
    },
    movieDelete: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2980b9',
        padding: 10,
        top: 10,
        bottom: 10,
        right: 10
    },
    movieDeleteText: {
        color: 'white'
    }
});