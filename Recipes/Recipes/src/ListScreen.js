import React from 'react';
import { Text, TextInput, View, Button, AppRegistry, AsyncStorage, ScrollView, StyleSheet, NetInfo } from 'react-native';
import { urls } from './constants';
import { Recipe } from './Recipe';
import { NavigationEvents } from 'react-navigation';

export class ListScreen extends React.Component {
    static navigationOptions = {
        title: 'Recipes',
    };

    user;
    constructor(props) {
        super(props);
        this.state = {
            credentials: {
                email: '',
                password: ''
            },
            recipes: [],
            networkConnected: true,
            offlineAddedNumber: 0
        }
        this.user = props.navigation.state.params.user;
    }
    componentDidMount() {
        this.props.navigation.addListener('willFocus', payload => {
            this.setRecipes();
        })

    }
    render() {
        let recipes = this.state.recipes;
        if (recipes) {
            recipes = recipes.map(recipe => {
                return <Recipe key={recipe._id} keyval={recipe}
                    deleteMethod={() => this.delete(recipe)} />
            });
        }
        return (
            <View style={styles.container}>
                <Button onPress={() => this.navigateToAdd()} title="Add recipe" />
                <ScrollView style={styles.scrollContainer}>
                    {recipes}
                </ScrollView>
            </View>
        );
    }

    setRecipes() {
        AsyncStorage.getItem('recipes').then(recipes => JSON.parse(recipes))
            .then(recipes => {
                if (recipes) {
                    this.setState({ recipes: recipes });
                }
            });
        NetInfo.isConnected.fetch().then(isConnected => {
            this.setState({ networkConnected: isConnected });
            if (isConnected) {
                this.syncData();
            }
        });
    }

    syncData() {
        AsyncStorage.getItem('add-recipes').then(response => JSON.parse(response))
            .then(recipes => {
                if (recipes) {
                    this.setState({ offlineAddedNumber: recipes.length });
                    this.syncOfflineRecipes(recipes);
                } else {
                    this.getRecipes();
                }
            })
    }

    syncOfflineRecipes(recipes) {
        for (const recipe of recipes) {
            fetch(urls.recipes, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${this.user.token}`
                },
                body: JSON.stringify(recipe)
            }).then(response => {
                const offlineAddedNumber = this.state.offlineAddedNumber;
                this.setState({ offlineAddedNumber: offlineAddedNumber - 1 });
                if (this.state.offlineAddedNumber === 0) {
                    this.getRecipes();
                }
            });
        }

    }
    navigateToAdd() {
        const { navigate } = this.props.navigation;
        navigate('Recipe');
    }
    async getRecipes() {
        await fetch(`${urls.recipes}?page=0&count=100`, {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.user.token}`
            }
        }).then(response => response.json())
            .then(recipes => {
                AsyncStorage.setItem('recipes', JSON.stringify(recipes));
                AsyncStorage.setItem('add-recipes', '');
                this.setState({ recipes: recipes });
            })
    }

    delete(recipe) {
        console.log(recipe);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#E91E63',
        borderBottomWidth: 10,
        borderBottomColor: '#ddd',
        flexDirection: 'row'
    },
    headerText: {
        color: 'white',
        fontSize: 18,
        padding: 20,

    },
    scrollContainer: {
        flex: 1,
        marginBottom: 100
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 10
    },
    textInput: {
        alignSelf: 'stretch',
        color: '#fff',
        padding: 20,
        backgroundColor: '#252525',
        borderTopWidth: 2,
        borderTopColor: '#ededed'
    },
    addButton: {
        position: 'absolute',
        zIndex: 11,
        right: 20,
        bottom: 90,
        backgroundColor: '#E91E63',
        width: 70,
        height: 70,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8
    },
    addButtonText: {
        color: '#fff',
        fontSize: 24
    },
    buttonLogout: {
        position: 'absolute',
        top: 0
    }
});

AppRegistry.registerComponent('Recipe', () => ListScreen);